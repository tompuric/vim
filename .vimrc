" Tab settings
set expandtab " tabs are spaces
set tabstop=4 " number of visual spaces per TAB
set softtabstop=4 " number of spaces in tab when editing


set shiftwidth=4

set autoindent
"set textwidth=80

set number " show line numbers
set showcmd " show command in bottom bar (right corner)
set cursorline " highlight current line 

"filetype indent on " load filetype-specific indent files. In ~/vim/indent/ you can add files such as 'python.vim' so that that specific vim file gets loaded and overrides the default settings for specific language/extension files. E.g. Force python extensions to use 2 spaces and tabs and what not.

set wildmenu " visual autocomplete for command menu. Press tab. I dare ya!
"set lazyredraw " redraw only when we need to. Useful for speeding up vim during macros
set showmatch " highlight matching [{()}]

" makes backspace work like most other apps
set backspace=indent,eol,start

" Colour 
syntax on
syntax enable " enable syntax processing
"colo darkblue
"colo peachpuff

let g:solarized_termcolors=256

set background=dark
colorscheme solarized
"colorscheme badwolf

" Key Mapping
ino jj <esc>
cno jj <c-c>

